module.exports = class MapBase {
  constructor () {
    this.map = {}
  }

  get (key) {
    return Object.keys(this.map[key] || {})
  }

  get raw () {
    return this.map
  }
}
