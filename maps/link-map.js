const set = require('lodash.set')
const clone = require('lodash.clone')

const BaseMap = require('./base-map')
const isRoot = require('../lib/is-root')
const isString = (str) => typeof str === 'string'

module.exports = class LinkMap extends BaseMap {
  constructor (nodes) {
    super()

    nodes
      .filter(node => !isRoot(node))
      .forEach(node => {
        node.previous.forEach(backlink => {
          set(this.map, [backlink, node.key], 1)
        })
      })

    // stored for #prune
    this.entryKeys = nodes
      .filter(node => isRoot(node))
      .map(node => node.key)

    /* may need this later if updating the graph */
    // this.getBacklinks = getBacklinks
  }

  remove (keys) {
    if (!Array.isArray(keys) || !keys.every(isString)) {
      throw new Error('@tangle/graph LinkMap expected keys of form [String]')
    }

    const pruneKeys = new Set(keys)
    // starting with the declared keys for pruning,
    // we determine which keys are downstream of that

    const queue = [...this.entryKeys]
    let key

    while (queue.length) {
      key = queue.pop()
      if (!this.map[key]) continue

      if (pruneKeys.has(key)) {
        forEachKey(this.map[key], linkedKey => {
          pruneKeys.add(linkedKey)
          queue.push(linkedKey)
        })
      } else {
        forEachKey(this.map[key], linkedKey => queue.push(linkedKey))
      }
    }

    const newMap = clone(this.map)

    // remove the top level invalid nodes
    pruneKeys.forEach(key => delete newMap[key])

    // remove the links to any invalid nodes
    forEachKey(newMap, fromNodeId => {
      forEachKey(newMap[fromNodeId], toNodeId => {
        if (pruneKeys.has(toNodeId)) delete newMap[fromNodeId][toNodeId]
      })

      // remove a this.map item if it has no remaining valid links
      if (isEmpty(newMap[fromNodeId])) delete newMap[fromNodeId]
    })

    this.map = newMap
    return [...pruneKeys]
  }
}

function forEachKey (object, cb) {
  Object.keys(object).forEach(key => cb(key))
}

function isEmpty (object) {
  return Object.keys(object).length === 0
}
