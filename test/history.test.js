const test = require('tape')
// const { buildGraph } = require('@tangle/test')
const Graph = require('..')

test('history', t => {
  // const mermaidGraph = buildGraph(`
  //   A-->B-->C-->D-->E-->F-->G
  //       B------>D------>F------>H
  // `)

  const A = { key: 'A', previous: null }
  const B = { key: 'B', previous: ['A'] }
  const C = { key: 'C', previous: ['A'] }
  const D = { key: 'D', previous: ['B', 'C'] }
  const Y = { key: 'Y', previous: ['X'] }
  const graph = new Graph([A, B, C, D, Y])

  t.deepEqual(
    graph.getHistory('A'),
    [],
    'Root node has no history'
  )

  t.deepEqual(
    graph.getHistory('D'),
    ['B', 'A', 'C'],
    'Merge node has both branches in history'
  )

  t.deepEqual(
    graph.getHistory('X'),
    [],
    'Node not in graph has no history'
  )
  t.deepEqual(
    graph.getHistory('Y'),
    [],
    'Disconnected node has no history'
  )

  //   t.deepEqual(
  //     mermaidGraph.getHistory('G'),
  //     ['F', 'E', 'D', 'B', 'A', 'C'],
  //     'Tip nodes have everything else in history'
  //   )
  // t.deepEqual(
  //   mermaidGraph.getHistory('H'),
  //   ['F', 'E', 'D', 'B', 'A', 'C'],
  //   'Tip nodes have everything else in history'
  // )
  t.end()
})
