const test = require('tape')
const Graph = require('..')

//     A   (root)
//    / \
//   B   C        --W (disconnected)
//    \ /           |
//     D            X (disconnected)

const A = { key: 'A', previous: null }
const B = { key: 'B', previous: ['A'] }
const C = { key: 'C', previous: ['A'] }
const D = { key: 'D', previous: ['B', 'C'] }

const W = { key: 'W', previous: ['V'] } // disconnected
const X = { key: 'X', previous: ['W'] } // disconnected
// const Z                                                     // exterior

const N = { key: 'N', previous: ['D', 'X'] }
const M = { key: 'M', previous: ['N'] }

const graph = new Graph([A, B, C, D, W, X])

graph.addNodes([N, M])

test('addNodes - adding two nodes at once', t => {
  t.deepEqual(new Graph([A, B, C, D, W, X, N, M]), graph,
    'Adding nodes afterwards is the same as including them initially')
  t.throws(
    () => graph.addNodes([N]),
    /each node to have a unique key/,
    'throws if non-unique node keys'
  )
  t.deepEqual(new Graph([A, B, C, D, W, X, N, M]), graph,
    'If theres an error when adding nodes it should try to revert the graph')
  t.end()
})

test('addNodes graph.rootKeys', t => {
  t.deepEqual(graph.rootKeys, ['A'], 'rootKeys')
  t.deepEqual(graph.rootNodeKeys, ['A'], 'alias: rootNodeKeys')

  t.end()
})

test('addNodes graph.rootNodes', t => {
  t.deepEqual(graph.rootNodes, [A], 'rootNodes')

  t.end()
})

test('addNodes graph.getNode', t => {
  t.equal(graph.getNode('A'), A, 'getNode')
  t.equal(graph.getNode('W'), null, 'disconnected node not returned')
  t.equal(graph.getNode('X'), null, 'disconnected node not returned')
  t.equal(graph.getNode('Z'), undefined, 'exterior node not return')
  t.equal(graph.getNode('N'), null, 'semiconnected node not found')
  t.equal(graph.getNode('M'), null, 'semiconnected node not found')
  t.end()
})

test('addNodes graph.getNext', t => {
  t.deepEqual(graph.getNext('A'), ['B', 'C'], 'getLinks')
  t.deepEqual(graph.getNext('B'), ['D'], 'getLinks')
  t.deepEqual(graph.getNext('C'), ['D'], 'getLinks')
  t.deepEqual(graph.getNext('W'), [], 'no links for disconnected')
  t.deepEqual(graph.getNext('X'), [], 'no links for disconnected')
  t.deepEqual(graph.getNext('Z'), [], 'no links for exterior')
  t.deepEqual(graph.getNext('N'), [], 'getLinks of semiconnected node')

  t.end()
})

test('addNodes graph.getPrevious', t => {
  /* getBacklinks */
  t.deepEqual(graph.getPrevious('B'), ['A'], 'getBacklinks')
  t.deepEqual(graph.getPrevious('W'), [], 'no backlinks for disconnected')
  t.deepEqual(graph.getPrevious('X'), [], 'no backlinks for disconnected')
  t.deepEqual(graph.getPrevious('Z'), [], 'no backlinks for exterior')
  t.deepEqual(graph.getPrevious('N'), [], 'getBacklinks of semiconnected node')

  t.end()
})

test('addNodes graph.isBranchNode', t => {
  /* isBranchNode */
  t.equal(graph.isBranchNode('A'), true, 'A is a branchNode')
  t.equal(graph.isBranchNode('B'), false, 'B not branchNode')
  t.equal(graph.isBranchNode('D'), false, 'D not branchNode')
  t.equal(graph.isBranchNode('W'), false, 'W not branchNode') // disconnected node
  t.equal(graph.isBranchNode('X'), false, 'X is not a branch node') // disconnected node
  t.equal(graph.isBranchNode('Z'), false, 'Z is not a branch node') // exterior node
  t.equal(graph.isBranchNode('N'), false, 'semiconnected node N not branchNode')

  t.end()
})

test('addNodes graph.isMergeNode', t => {
  t.equal(graph.isMergeNode('A'), false, 'A not merge node')
  t.equal(graph.isMergeNode('B'), false, 'B not merge node')
  t.equal(graph.isMergeNode('D'), true, 'D is a merge node')
  t.equal(graph.isMergeNode('X'), false, 'X not merge node') // disconnected
  t.equal(graph.isMergeNode('Y'), false, 'Y not merge node') // disconnected node
  t.equal(graph.isMergeNode('Z'), false, 'Z not merge node') // exterior node
  t.equal(graph.isMergeNode('N'), false, 'semiconnected node N not a merge node (despite having multiple previous nodes)')

  t.end()
})

test('addNodes - graph.isTipNode', t => {
  t.equal(graph.isTipNode('A'), false, 'A not tipNode')
  t.equal(graph.isTipNode('B'), false, 'B not tipNode')
  t.equal(graph.isTipNode('D'), true, 'D is still tipNode')
  t.equal(graph.isTipNode('W'), false, 'W not tipNode') // disconnected
  t.equal(graph.isTipNode('X'), false, 'X not tipNode') // disconnected
  t.equal(graph.isTipNode('Z'), false, 'Z not tipNode') // exterior node
  t.equal(graph.isTipNode('M'), false, 'semiconnected node M is not tipNode')

  t.end()
})

// Testing rejoining disconnected nodes
//  D1
//  |
//  D2 (missing initially)
//  |
// D3
const D1 = { key: 'D1', previous: null }
const D2 = { key: 'D2', previous: ['D1'] }
const D3 = { key: 'D3', previous: ['D2'] }

test('addNodes - disconnected Graph', t => {
  const dGraph = new Graph([D1, D3])
  dGraph.addNodes([D2])
  t.deepEqual(new Graph([D1, D3, D2]), dGraph,
    'Adding nodes afterwards is the same as including them initially')
  // Note, graph may be stored in a different order, new Graph([D1,D2,D3]) fails

  t.equal(dGraph.isTipNode('D3'), true, 'D3 is headNode')
  t.equal(dGraph.getNode('D3'), D3, 'previously disconnected node found')
  t.deepEqual(dGraph.getPrevious('D3'), ['D2'], 'getBacklinks of previously disconnected node')

  t.end()
})

test('addNodes - calling addNodes in succession', t => {
  const gGraph = new Graph([A])
  gGraph.addNodes([B])
  t.deepEqual(new Graph([A, B]), gGraph, 'Adding 1 node')
  gGraph.addNodes([C, D])
  t.deepEqual(new Graph([A, B, C, D]), gGraph, 'Then adding 2 nodes')

  gGraph.addNodes([W, M])
  t.deepEqual(new Graph([A, B, C, D, W, M]), gGraph, 'Then adding 2 nodes')

  gGraph.addNodes([N, X])
  t.deepEqual(new Graph([A, B, C, D, W, M, N, X]), gGraph, 'Then adding 2 nodes')

  t.equal(gGraph.isTipNode('D'), true, 'D is tip')

  t.end()
})

test('addNodes - adding a lot of nodes', t => {
  const gGraph = new Graph([A])
  gGraph.addNodes([B, C, D, M, W, X, N])
  t.deepEqual(new Graph([A, B, C, D, M, W, X, N]), gGraph, 'Adding lots of nodes')

  t.equal(gGraph.isTipNode('D'), true, 'D is tip')

  t.end()
})

// When testing the speed of addNodes use test.only and increase howManyNodes
const howManyNodes = 10

const A1 = { key: 'A1', previous: null }

test('addNodes - adding lots of nodes', t => {
  const graph1 = new Graph([A1])
  let prev = 'A1'
  let i
  for (i = 0; i < howManyNodes; i++) {
    const keyString = i.toString(10)
    const newNode = { key: keyString, previous: [prev] }
    graph1.addNodes([newNode])
    prev = keyString
  }

  const graph2 = new Graph([A1])

  const arrayOfNodes = []
  let prev2 = 'A1'
  for (i = 0; i < howManyNodes; i++) {
    const keyString2 = i.toString(10)
    const newNode2 = { key: keyString2, previous: [prev2] }
    arrayOfNodes.push(newNode2)
    prev2 = keyString2
  }

  graph2.addNodes(arrayOfNodes)

  t.deepEqual(graph1, graph2,
    'Adding the nodes all at once or one at a time has the same result')
  t.end()
})
