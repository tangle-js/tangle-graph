const test = require('tape')
const { LinkMap } = require('../maps')

test('LinkMap: linear', t => {
  //    A   (root)
  //    |
  //    B
  //    |
  //    C

  const A = { key: 'A', previous: null }
  const B = { key: 'B', previous: ['A'] }
  const C = { key: 'C', previous: ['B'] }

  const expectedMap = {
    A: { B: 1 },
    B: { C: 1 }
  }

  t.deepEqual(new LinkMap([A, B, C]).raw, expectedMap, 'simple linear')
  t.deepEqual(new LinkMap([C, B]).raw, expectedMap, 'simple linear (order agnostic)')

  // ## message in-thread but "dangling" (doesn't link up to known messages)
  //
  t.end()
})

test('LinkMap: merge', t => {
  //     A   (root)
  //    / \
  //   B   C
  //    \ /
  //     D

  const A = { key: 'A', previous: null }
  const B = { key: 'B', previous: ['A'] }
  const C = { key: 'C', previous: ['A'] }
  const D = { key: 'D', previous: ['B', 'C'] }

  const linkMap = new LinkMap([A, D, B, C])
  const expectedMap = {
    A: { B: 1, C: 1 },
    B: { D: 1 },
    C: { D: 1 }
  }

  t.deepEqual(linkMap.raw, expectedMap, 'raw graph: happy')

  t.deepEqual(linkMap.get('A'), ['B', 'C'], 'get: works')
  t.deepEqual(linkMap.get('D'), [], 'get: works (head)')
  t.end()
})

test('LinkMap: dangle', t => {
  //    A   (root)
  //    |
  //    B     ----?--- J? (a message we don't have)
  //    |              |
  //    C              K

  const A = { key: 'A', previous: null }
  const B = { key: 'B', previous: ['A'] }
  const C = { key: 'C', previous: ['B'] }
  const K = { key: 'K', previous: ['J'] }

  const expectedMap = {
    A: { B: 1 },
    B: { C: 1 },
    J: { K: 1 }
  }
  t.deepEqual(new LinkMap([A, B, C, K]).raw, expectedMap, 'dangles')

  t.end()
})

test('LinkMap: non-thread dangles', t => {
  //    A (root)           R?  (root, some other thread)
  //    |                   ?
  //    B                   S (a message we don't have)
  //    |                   |
  //    C                   Q

  const A = { key: 'A', previous: null }
  const B = { key: 'B', previous: ['A'] }
  const C = { key: 'C', previous: ['B'] }
  const Q = { key: 'Q', previous: ['S'] }

  const expectedMap = {
    A: { B: 1 },
    B: { C: 1 },
    S: { Q: 1 }
  }

  t.deepEqual(new LinkMap([A, B, C, Q]).raw, expectedMap, 'out of thread messages')
  t.end()
})

test('LinkMap: complex merge', t => {
  //      A  (root)
  //     / \
  //    B   C
  //   /   / \
  //  D   E   F
  //   \ /     \
  //    H       G
  //    |
  //    I

  const A = { key: 'A', previous: null }
  const B = { key: 'B', previous: ['A'] }
  const C = { key: 'C', previous: ['A'] }
  const D = { key: 'D', previous: ['B'] }
  const E = { key: 'E', previous: ['C'] }
  const F = { key: 'F', previous: ['C'] }
  const H = { key: 'H', previous: ['D', 'E'] }
  const G = { key: 'G', previous: ['F'] }
  const I = { key: 'I', previous: ['H'] }

  const expectedMap = {
    A: { B: 1, C: 1 },
    B: { D: 1 },
    C: { E: 1, F: 1 },
    D: { H: 1 },
    E: { H: 1 },
    F: { G: 1 },
    H: { I: 1 }
  }

  t.deepEqual(new LinkMap([A, B, C, C, D, E, F, G, G, H, I]).raw, expectedMap, 'ugly graph')

  t.end()
})

test('LinkMap: custom thread path', t => {
  //      A  (root)
  //     / \
  //    B   C

  const A = { key: 'A', previous: null }
  const B = { key: 'B', previous: ['A'] }
  const C = { key: 'C', previous: ['A'] }

  const expectedMap = {
    A: { B: 1, C: 1 }
  }

  t.deepEqual(new LinkMap([A, B, C]).raw, expectedMap, 'custom thread path')

  t.end()
})

//      A  (root)
//     / \
//    B   C
//   /   / \
//  D   E   F
//   \ /     \
//    H       G
//    |
//    I

const mockLinkMap = () => {
  const nodes = [
    { key: 'A', previous: null },
    { key: 'B', previous: ['A'] },
    { key: 'C', previous: ['A'] },
    { key: 'D', previous: ['B'] },
    { key: 'E', previous: ['C'] },
    { key: 'F', previous: ['C'] },
    { key: 'G', previous: ['F'] },
    { key: 'H', previous: ['D', 'E'] },
    { key: 'I', previous: ['H'] }
  ]

  return new LinkMap(nodes)
}

test('LinkMap#remove', t => {
  const map1 = mockLinkMap()
  // say C is invalid, result:
  //
  //      A  (root)
  //     /
  //    B
  //   /
  //  D
  const removed1 = map1.remove(['C']).sort()
  t.deepEqual(
    {
      map: map1.raw,
      removed: removed1
    },
    {
      map: {
        A: { B: 1 },
        B: { D: 1 }
      },
      removed: ['C', 'E', 'F', 'G', 'H', 'I']
    },
    'simple remove'
  )

  const map2 = mockLinkMap()
  // say C + H are invalid, result:
  //
  //      A  (root)
  //     /
  //    B
  //   /
  //  D
  const removed2 = map2.remove(['C', 'H']).sort()
  t.deepEqual(
    {
      map: {
        A: { B: 1 },
        B: { D: 1 }
      },
      removed: ['C', 'E', 'F', 'G', 'H', 'I']
    },
    {
      map: map2.raw,
      removed: removed2
    },
    'simple remove (redundent invalid nodes)'
  )

  const map3 = mockLinkMap()
  // say D is removed, result :
  //
  //      A  (root)
  //     / \
  //    B   C
  //       / \
  //      E   F
  //           \
  //            G
  const removed3 = map3.remove(['D']).sort()
  t.deepEqual(
    {
      map: {
        A: { B: 1, C: 1 },
        C: { E: 1, F: 1 },
        F: { G: 1 }
      },
      removed: ['D', 'H', 'I']
    },
    {
      map: map3.raw,
      removed: removed3
    },
    'a different invalidation'
  )

  t.end()
})
