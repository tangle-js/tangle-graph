const assert = require('assert').strict

module.exports = function isRoot (node) {
  assert(node.key)

  return node.previous === null
}
